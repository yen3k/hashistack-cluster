# Enable node drain

```
nomad node drain -enable -deadline 30m -self
```

# Disable node drain

```
nomad node drain -disable -self
```
