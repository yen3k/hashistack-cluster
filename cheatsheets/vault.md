Get KV data of key

```
vault kv get secret/example
```

Get single value from KV data key

```
vault kv get -field=examplekey secret/example
```

Patch KV data

```
vault kv patch secret/example newkey=newvalue
```
