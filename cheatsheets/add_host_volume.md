# Server

## NFS

Create new dir:

```
mkdir -p /srv/nfs/<NAME>/ /mnt/storage/<NAME>/
```

Bind:

```
mount --bind /mnt/storage/<NAME>/ /srv/nfs/<NAME>/
```

Add to `/etc/fstab`:

```
/mnt/storage/<NAME>/ /srv/nfs/<NAME>/ none bind 0 0
```

Add to `/etc/exports`:

```
/srv/nfs/<NAME> 192.168.1.0/24(rw,sync,no_root_squash)
```

Reload exports:

```
exportfs -avr
```

## Nomad configuration

Add new host volume under client stanza:

```
host_volume "<NAME>-volume" {
  path      = "/mnt/storage/<NAME>/"
  read_only = false
}
```

Reload Nomad:

```
systemctl restart nomad
```

# Client

## NFS

Create directory:

```
mkdir -p /mnt/storage/<NAME>/
```

Mount:

```
mount -t nfs -o vers=4 cluster-s1:/srv/nfs/<NAME>/ /mnt/storage/<NAME>/
```

Add entry to `/etc/fstab`:

```
cluster-s1:/srv/nfs/<NAME>/   /mnt/storage/<NAME>/   nfs   defaults,timeo=900,retrans=5,_netdev  0 0
```

## Nomad configuration

Add new host volume under client stanza:

```
host_volume "<NAME>-volume" {
  path      = "/mnt/storage/<NAME>/"
  read_only = false
}
```

Reload Nomad:

```
systemctl restart nomad
```
