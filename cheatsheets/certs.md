# PUBLIC CERTIFICATE (LETS ENCRYPT)

## Add certificate to HAProxy

MAKE SURE PORT FORWARDING IS CONFIGURED TO PORT 80

Create certificate

```
certbot certonly --standalone -d <DOMAIN>
```

Collect and move to NFS

```
cat /etc/letsencrypt/live/<DOMAIN>/fullchain.pem /etc/letsencrypt/live/<DOMAIN>/privkey.pem | tee /mnt/storage/certs/<DOMAIN>.pem
```

## Add to active_certs

Add domain name to `scripts/active_public_certs`

## Update Certificate

```
certbot renew
cat /etc/letsencrypt/live/<DOMAIN>/fullchain.pem /etc/letsencrypt/live/<DOMAIN>/privkey.pem | tee /mnt/storage/certs/<DOMAIN>.pem
```

## Delete certificate

```
certbot delete --cert-name <DOMAIN>
```

# PRIVATE CERTIFICATE

## Generate CA

Create password encrypted RSA-key (STORE SAFELY)

```
openssl genrsa -aes256 -out ca-key.pem 4096
```

Generate CA certificate

```
openssl req -new -x509 -sha256 -days 365 -key ca-key.pem -out ca.pem
```

## Generate Certificate

Create unencryptet RSA-key

```
openssl genrsa -out cert-key.pem 4096
```

Create certificate signing request (CSR)

```
openssl req -new -sha256 -subj "/CN=<COMMON_NAME>" -key cert-key.pem -out cert.csr
```

Create certificate configuration

```
echo "subjectAltName=DNS:<DOMAIN>,IP:<IP>" >> extfile.cnf
```

Create certificate
```
openssl x509 -req -sha256 -days 365 -in cert.csr -CA ca.pem -CAkey ca-key.pem -out cert.pem -extfile extfile.cnf -CAcreateserial
```

Test if this works instead of extfile: `-addext "subjectAltName = DNS:foo.co.uk"`

## Prepare for HAProxy

```
cat cert.pem ca.pem cert-key.pem >> DOMAIN
```

Upload to HAProxy certificate directory: `/mnt/storage/certs/`

## Renew certificate

https://www.golinuxcloud.com/renew-self-signed-certificate-openssl/
