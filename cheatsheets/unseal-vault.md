# Unseal Vault

```
export VAULT_TOKEN=<ROOT_TOKEN>
vault operator unseal
vault operator unseal
vault operator unseal
vault login # root-token
export VAULT_TOKEN=$(vault token create -policy nomad-server -period 72h -orphan | awk '{print $2}' | sed -n '3p')
vault login # nomad-token
```
