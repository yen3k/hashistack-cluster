Home router forwarding:

```
dig postgres.service.dc1.consul
```

Directly:

```
dig -p 8600 @yenclust-s1 postgres.service.dc1.consul
```
