datacenter = "dc1"
data_dir   = "/opt/nomad"
advertise {
  http = "{{ GetInterfaceIP \"end0\" }}"
  rpc  = "{{ GetInterfaceIP \"end0\" }}"
  serf = "{{ GetInterfaceIP \"end0\" }}"
}
server {
  enabled          = true
  bootstrap_expect = 1
}

client {
  enabled = true
  host_volume "certs-volume" {
    path      = "/mnt/storage/certs/"
    read_only = false
  }
  host_volume "sesh-secretary-volume" {
    path      = "/mnt/storage/seshsecretary/"
    read_only = false
  }
  host_volume "yenting-webserver-volume" {
    path      = "/mnt/storage/yentingwebserver/"
    read_only = false
  }
  host_volume "owncloud-volume" {
    path      = "/mnt/storage/owncloud/"
    read_only = false
  }
  host_volume "postgres-volume" {
    path      = "/mnt/storage/postgres/"
    read_only = false
  }
  host_volume "sesh-remindme-volume" {
    path      = "/mnt/storage/seshremindme/"
    read_only = false
  }
    host_volume "mumble-volume" {
    path      = "/mnt/storage/mumble/"
    read_only = false
  }
  host_volume "apidoc-volume" {
    path      = "/mnt/storage/apidoc/"
    read_only = false
  }
  host_volume "gitea-volume" {
    path      = "/mnt/storage/gitea/"
    read_only = false
  }
  host_volume "jellyfin-cache-volume" {
    path      = "/mnt/storage/jellyfin/cache"
    read_only = false
  }
  host_volume "jellyfin-config-volume" {
    path      = "/mnt/storage/jellyfin/config"
    read_only = false
  }
  host_volume "jellyfin-media-volume" {
    path      = "/mnt/storage/jellyfin/media"
    read_only = false
  }
  host_volume "dev-hiive-releases-volume" {
    path      = "/mnt/storage/dev-hiive-releases"
    read_only = false
  }
}

vault {
  enabled = true
  address = "http://vault.service.consul:8200"
  token = "<TOKEN>"
}
