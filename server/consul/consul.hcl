datacenter     = "dc1"
data_dir       = "/opt/consul"
bind_addr      = "{{ GetInterfaceIP \"end0\" }}"
advertise_addr = "{{ GetInterfaceIP \"end0\" }}"
client_addr    = "0.0.0.0"
ports {
}
addresses {
}
ui_config {
  enabled = true
}
server           = true
bootstrap_expect = 1
