# Run base.sh provisioning script first
# Run as root

TEMP_PATH='/tmp/setup/'

CONFIG_URL='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/server/'

BASH_CONF='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/server/.bashrc'
FSTAB_CONF='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/server/provision/files/fstab'
NFS_CONF='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/server/provision/files/exports'

NOMAD_VERSION='1.7.2'
NOMAD_CONF='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/server/nomad/nomad.hcl'
NOMAD_SYSTEMD='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/server/provision/files/nomad.service'

CONSUL_VERSION='1.17.1'
CONSUL_CONF='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/server/consul/consul.hcl'
CONSUL_SYSTEMD='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/server/provision/files/consul.service'

VAULT_VERSION='1.15.4'
VAULT_CONF='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/server/vault/vault.hcl'
VAULT_SYSTEMD='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/server/provision/files/vault.service'

BACKUP_SYSTEMD='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/server/provision/files/backup.service'
BACKUP_SYSTEMDTIMER_='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/server/provision/files/backup.timer'

pacman -S python-pip python-pipx librsync nfs-utils docker certbot && \
  mkdir -p /mnt/storage && \
  mkdir -p /mnt/backup && \
  mkdir -p /srv/nfs/seshremindme && \
  mkdir -p /srv/nfs/seshsecretary && \
  mkdir -p /srv/nfs/yentingwebserver && \
  wget $BASH_CONF -O /root/.bashrc && \
  wget $FSTAB_CONF -O /etc/fstab && \
  wget $NFS_CONF -O /etc/exports && \
  systemctl enable docker && \
  systemctl enable nfs-server && \
  pipx install rdiff-backup && \
  pipx ensurepath && \

# Nomad

mkdir -p /tmp/setup/ && \
  wget https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_arm64.zip -P $TEMP_PATH && \
  unzip ${TEMP_PATH}nomad_${NOMAD_VERSION}_linux_arm64.zip -d /usr/local/bin/ && \
  mkdir -p /etc/nomad.d/ && \
  mkdir -p /etc/nomad.d/config && \
  wget $NOMAD_CONF -P /etc/nomad.d/ && \
  wget $NOMAD_SYSTEMD -P /etc/systemd/system && \

# Consul

mkdir -p /tmp/setup/ && \
  useradd --system --home /etc/consul.d --shell /bin/false consul && \
  wget https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_arm64.zip -P $TEMP_PATH && \
  unzip ${TEMP_PATH}consul_${CONSUL_VERSION}_linux_arm64.zip -d /usr/local/bin/ && \
  mkdir -p /etc/consul.d/ && \
  mkdir -p /etc/consul.d/config && \
  mkdir -p /opt/consul && \
  chown consul:consul /opt/consul && \
  wget $CONSUL_CONF -P /etc/consul.d/ && \
  wget $CONSUL_SYSTEMD -P /etc/systemd/system/ && \

# Vault

mkdir -p /tmp/setup/ && \
  useradd --system --home /etc/vault.d --shell /bin/false vault && \
  wget https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_arm64.zip -P $TEMP_PATH && \
  unzip ${TEMP_PATH}vault_${VAULT_VERSION}_linux_arm64.zip -d /usr/local/bin && \
  mkdir -p /etc/vault.d/ && \
  wget $VAULT_CONF -P /etc/vault.d/ && \
  wget $VAULT_SYSTEMD -P /etc/systemd/system/ && \
  chown -R vault:vault /etc/vault.d/ && \

# Backup job
wget $BACKUP_SYSTEMD -P /etc/systemd/system/ && \
wget $BACKUP_SYSTEMD_TIMER -P /etc/systemd/system/ && \

systemctl enable nomad && \
  systemctl enable consul && \
  systemctl enable vault && \
  systemctl enable backup.timer && \

# SETUP VAULT MANUALLY
