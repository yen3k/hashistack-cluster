# Backup

Install `rdiff-backup`

```
pacman -S rdiff-backup
```

# Host volume

Add drive to `/etc/fstab` and mount on `/mnt/storage`

```
pacman -Sy nfs-utils
systemctl enable nfs-server.service
systemctl start nfs-server.service
```

# Backup volume

Add drive to `/etc/fstab` and mount on `/mnt/backup`

# Docker

```
pacman -Sy docker
systemctl enable docker
systemctl start docker
```

# Certificates

```
pacman -Sy certbot
```

Create cert directories and bind

```
mkdir -p /srv/nfs/certs /mnt/storage/certs
mount --bind /mnt/storage/certs /srv/nfs/certs
```

Add entry to `/etc/fstab`:

```
/mnt/storage/certs /srv/nfs/certs none bind 0 0
```

Add NFS export to `/etc/exports`:

```
/srv/nfs/certs 192.168.1.0/24(rw,sync,no_root_squash)
```

Reload NFS exports:

```
exportfs -avr
```


# Install `hashi-up`

```
curl -sLS https://get.hashi-up.dev | sudo sh
```

# Consul

```
hashi-up consul install \
  --ssh-target-addr yenclust-s1.lan \
  --ssh-target-key /home/yen/.ssh/id_rsa \
  --server \
  --advertise-addr "{{ GetInterfaceIP \"eth0\" }}" \
  --bind-addr "{{ GetInterfaceIP \"eth0\" }}" \
  --client-addr 0.0.0.0
```

# Nomad

```
hashi-up nomad install \
  --ssh-target-addr yenclust-s1 \
  --ssh-target-key /home/yen/.ssh/id_rsa \
  --server \
  --advertise "{{ GetInterfaceIP \"eth0\" }}" \
  --bootstrap-expect 1
```

# Vault

```
hashi-up vault install \
  --ssh-target-addr yenclust-s1 \
  --ssh-target-key /home/yen/.ssh/id_rsa \
  --storage consul \
  --key-file=server-key.pem \
  --cert-file=server.pem
```

Initialize

```
vault operator init
```

or follow setup at `http://yenclust-s1:8200`

Add to `.bashrc`

```
export VAULT_ADDR=http://127.0.0.1:8200
export VAULT_TOKEN=<TOKEN>
```
