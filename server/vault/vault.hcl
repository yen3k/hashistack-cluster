ui = true
cluster_addr  = "https://127.0.0.1:8201"
api_addr      = "https://127.0.0.1:8200"
disable_mlock = true

service_registration "consul" {
  address = "127.0.0.1:8500"
}

storage "raft" {
    path = "/etc/vault.d/"
    node_id = "cluster-s1"
}
listener "tcp" {
  address     = "0.0.0.0:8200"
  tls_disable = true
}
