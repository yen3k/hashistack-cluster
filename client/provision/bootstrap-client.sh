#!/bin/bash

# Run base.sh provisioning script first
# Run as root

BASH_CONF_URL='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/client/.bashrc'
FSTAB_URL='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/client/provision/files/fstab'
NOMAD_ZIP_PATH='https://releases.hashicorp.com/nomad/1.7.2/'
NOMAD_ZIP_FILE='nomad_1.7.2_linux_arm64.zip'
CONSUL_ZIP_PATH='https://releases.hashicorp.com/consul/1.17.1/'
CONSUL_ZIP_FILE='consul_1.17.1_linux_arm64.zip'
NOMAD_CONF_URL='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/client/nomad/nomad.hcl'
CONSUL_CONF_URL='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/client/consul/consul.hcl'
SYSTEMD_NOMAD_URL='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/client/provision/files/nomad.service'
SYSTEMD_CONSUL_URL='https://gitlab.com/yen3k/hashistack-cluster/-/raw/main/client/provision/files/consul.service'

pacman -Sy --noconfirm --needed nfs-utils docker && \
  systemctl enable nfs-client.target && \
  systemctl enable docker && \
  wget $BASH_CONF_URL -O /root/.bashrc && \
  wget $FSTAB_URL -O /etc/fstab && \
  mkdir -p /mnt/storage/seshremindme && \
  mkdir -p /mnt/storage/seshsecretary && \
  mkdir -p /mnt/storage/yentingwebserver && \
  mkdir -p /mnt/storage/apidoc && \
  mkdir -p /mnt/storage/mumble && \

  wget $NOMAD_ZIP_PATH$NOMAD_ZIP_FILE -P /tmp && \
  unzip /tmp/$NOMAD_ZIP_FILE -d /usr/local/bin/ && \
  mkdir -p /etc/nomad.d/ && \
  mkdir -p /etc/nomad.d/config && \
  wget $NOMAD_CONF_URL -P /etc/nomad.d/ && \
  wget $SYSTEMD_NOMAD_URL -P /etc/systemd/system/ && \

  useradd --system --home /etc/consul.d --shell /bin/false consul && \
  wget $CONSUL_ZIP_PATH$CONSUL_ZIP_FILE -P /tmp && \
  unzip /tmp/$CONSUL_ZIP_FILE -d /usr/local/bin/ && \
  mkdir -p /etc/consul.d/ && \
  mkdir -p /opt/consul/ && \
  mkdir -p /etc/consul.d/config && \
  chown consul:consul /opt/consul/ && \
  wget $CONSUL_CONF_URL -P /etc/consul.d/ && \
  wget $SYSTEMD_CONSUL_URL -P /etc/systemd/system/ && \
  chown -R consul:consul /etc/consul.d && \

  systemctl enable nomad.service && \
  systemctl enable consul.service
