# nfs

```
pacman -Sy nfs-utils
systemctl enable nfs-client.target
systemctl start nfs-client.target
```

Mount nfs:

```
mkdir -p /mnt/storage/<PROGRAM>/
mount -t nfs -o vers=4 yenclust-s1:/srv/nfs/<PROGRAM>/ /mnt/storage/<PROGRAM>/
```

Add entry to `/etc/fstab`

```
yenclust-s1:/srv/nfs/<PROGRAM>/   /mnt/storage/<PROGRAM>/   nfs   defaults,timeo=900,retrans=5,_netdev  0 0
```

# Docker

```
pacman -Sy docker
systemctl enable docker
systemctl start docker
```

# Install `hashi-up`

```
curl -sLS https://get.hashi-up.dev | sudo sh
```

# Consul

```
hashi-up consul install \
  --ssh-target-addr yenclust-c1.lan \
  --ssh-target-key /home/yen/.ssh/id_rsa \
  --advertise-addr "{{ GetInterfaceIP \"eth0\" }}" \
  --bind-addr "{{ GetInterfaceIP \"eth0\" }}" \
  --retry-join yenclust-s1.lan
```

# Nomad

```
hashi-up nomad install \
  --ssh-target-addr yenclust-c1 \
  --ssh-target-key /home/yen/.ssh/id_rsa \
  --client \
  --advertise "{{ GetInterfaceIP \"eth0\" }}"
```

