datacenter     = "dc1"
data_dir       = "/opt/consul"
bind_addr      = "{{ GetInterfaceIP \"end0\" }}"
advertise_addr = "{{ GetInterfaceIP \"end0\" }}"
retry_join     = ["cluster-s1.lan"]
ports {
}
addresses {
}
