job "sesh-secretary" {

  datacenters = ["dc1"]

  group "sesh-secretary" {
    count = 1

    volume "sesh-secretary-volume" {
      type = "host"
      source = "sesh-secretary-volume"
    }

    task "sesh-secretary" {
      driver = "docker"

      config {
        image = "yen3k/sesh-secretary:latest"
      }

      constraint {
        attribute = "${attr.unique.hostname}"
        operator = "=="
        value = "cluster-s1"
      }

      template {
        data = <<EOF
SESHSECRETARY_TOKEN="{{ with secret "secret/data/seshsecretary" }}{{ .Data.data.token }}{{ end }}"
EOF
        destination = "secrets/file.env"
        env         = true
      }

      volume_mount {
        volume = "sesh-secretary-volume"
        destination = "/var/lib/seshsecretary"
      }

      vault {
        policies = ["default", "sesh-secretary"]
      }

      service {
        name = "sesh-secretary"
        tags = ["sesh"]
      }
    }
  }
}
