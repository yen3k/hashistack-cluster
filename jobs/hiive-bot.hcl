job "hiive-bot" {
  datacenters = ["dc1"]

  group "hiive-bot" {
    count = 1

    network {
      port "http" { }
    }

    task "hiive-bot" {
      driver = "docker"

      constraint {
        attribute = "${attr.unique.hostname}"
        operator = "=="
        value = "cluster-s1"
      }

      config {
        image = "yen3k/hiive-bot:latest"
        ports = ["http"]
        tty   = true
      }

      env {
        WEB_PORT = "${NOMAD_PORT_http}"
      }

      template {
        data = <<EOF
HIIVE_BOT_TOKEN="{{ with secret "secret/data/hiivebot" }}{{ .Data.data.token }}{{ end }}"
HIIVE_BOT_PASSWORD="{{ with secret "secret/data/hiivebot" }}{{ .Data.data.password }}{{ end }}"
EOF
        destination = "secrets/file.env"
        env         = true
      }

      vault {
        policies = ["default", "hiive-bot"]
      }

      service {
        name = "hiive-bot"
        tags = ["hiive"]
        port = "http"

        check {
          type     = "http"
          path     = "/bot/health_check"
          interval = "2m"
          timeout  = "2s"
        }
      }
    }
  }
}
