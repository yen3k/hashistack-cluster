job "dev-hiive" {
  datacenters = ["dc1"]

  group "dev-hiive" {
    count = 1

    volume "dev-hiive-releases-volume" {
      type = "host"
      source = "dev-hiive-releases-volume"
    }

    volume "apidoc-volume" {
      type = "host"
      source = "apidoc-volume"
    }    

    network {
      port "api-http" { }
      port "releases-http" { }
      port "readings-http" { }
      port "doc-http" { }
    }

    task "dev-hiive-api" {
      driver = "docker"

      config {
        image = "yen3k/dev-hiive-api:latest"
        ports = ["api-http"]
      }

      constraint {
        attribute = "${attr.unique.hostname}"
        operator = "=="
        value = "cluster-s1"
      }

      env {
        APP_ENVIRONMENT="development"
        APP_APPLICATION__PORT ="${NOMAD_PORT_api-http}"
        APP_DATABASE__DATABASE_NAME="devhiive"
        APP_DATABASE__USERNAME="devhiive"
        APP_DATABASE__REQUIRE_SSL=false
        APP_APPLICATION__ACCESS_TOKEN_DURATION=5
        APP_APPLICATION__REFRESH_TOKEN_DURATION=10
        APP_APPLICATION__BASE_URL="https://dev.api.hiive.sesh.jetzt"
        APP_EMAIL_CLIENT__SENDER_EMAIL="hiive@yenmail.party"
      }

      template {
        data = <<EOF
APP_APPLICATION__SECRET_KEY="{{ with secret "secret/data/devhiive" }}{{ .Data.data.secretkey }}{{ end }}"
APP_DATABASE__PASSWORD="{{ with secret "secret/data/devhiive" }}{{ .Data.data.pgpassword }}{{ end }}"
APP_DATABASE__HOST="{{ range service "postgres"}}{{.Address}}{{ end }}"
APP_DATABASE__PORT="{{ range service "postgres"}}{{.Port}}{{ end }}"
APP_EMAIL_CLIENT__AUTHORIZATION_TOKEN="{{ with secret "secret/data/devhiive" }}{{ .Data.data.postmarktoken }}{{ end }}"
EOF
        destination = "secrets/file.env"
        env         = true
      }

      vault {
        policies = ["default", "devhiive"]
      }

      service {
        name = "dev-hiive-api"
        port = "api-http"

        check {
          type     = "http"
          path     = "/health_check"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }

    task "dev-hiive-releases-webserver" {
      driver = "docker"

      config {
        image = "joseluisq/static-web-server:2.24.2-alpine"
        ports = [ "releases-http" ]

        volumes = [
          "local/config.toml:/config.toml",
        ]
      }

      env {
        SERVER_PORT = "${NOMAD_PORT_releases-http}"
        SERVER_ROOT = "/public"
        SERVER_CONFIG_FILE = "/config.toml"
      }

      template {
        data = <<EOF
[general]

#### Logging
log-level = "info"

#### Auto Compression
compression = false

#### Directory listing
directory-listing = true

#### Worker threads
threads-multiplier = 1

#### Log request Remote Address if available
log-remote-address = true

#### Health-check endpoint (GET or HEAD `/health`)
health = true

#### MD5 HASHES
[[advanced.headers]]
source = "/gateway/0.2*"
headers = { x-MD5 = "3232af673ac38cd79b8b0e56a0aedd24" }

[[advanced.headers]]
source = "/gateway/blink*"
headers = { x-MD5 = "931e6eed60939b785486bfb277f030ec" }

[[advanced.headers]]
source = "/gateway/wrongmd5*"
headers = { x-MD5 = "5d298c1670bd5db6a63db8146f409c51" }
EOF
        destination = "local/config.toml"
        env         = false
      }

      volume_mount {
        volume = "dev-hiive-releases-volume"
        destination = "/public"
      }


      service {
        name = "dev-hiive-releases-webserver"
        port = "releases-http"

        check {
          type     = "http"
          path     = "/health"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }

    task "dev-hiive-readings" {
      driver = "docker"

      config {
        image = "joseluisq/static-web-server:2.24.2-alpine"
        ports = [ "readings-http" ]

        volumes = [
          "local/config.toml:/config.toml",
        ]
      }

      env {
        SERVER_PORT = "${NOMAD_PORT_readings-http}"
        SERVER_ROOT = "/public"
        SERVER_CONFIG_FILE = "/config.toml"
      }

      template {
        data = <<EOF
[general]

#### Logging
log-level = "info"

#### Auto Compression
compression = false

#### Directory listing
directory-listing = true

#### Worker threads
threads-multiplier = 1

#### Log request Remote Address if available
log-remote-address = true

#### Health-check endpoint (GET or HEAD `/health`)
health = true

#### MD5 HASHES
[[advanced.headers]]
source = "/gateway/0.2*"
headers = { x-MD5 = "3232af673ac38cd79b8b0e56a0aedd24" }

[[advanced.headers]]
source = "/gateway/blink*"
headers = { x-MD5 = "931e6eed60939b785486bfb277f030ec" }

[[advanced.headers]]
source = "/gateway/wrongmd5*"
headers = { x-MD5 = "5d298c1670bd5db6a63db8146f409c51" }
EOF
        destination = "local/config.toml"
        env         = false
      }

      volume_mount {
        volume = "dev-hiive-releases-volume"
        destination = "/public"
      }


      service {
        name = "dev-hiive-readings"
        port = "readings-http"
        
        check {
          type     = "http"
          path     = "/health"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }

    task "hiive-api-doc-website" {
      driver = "docker"

      config {
        image = "yen3k/yenting-webserver:latest"
        ports = ["doc-http"]
        tty   = true
      }

      env {
        WEB_PORT = "${NOMAD_PORT_doc-http}"
      }

      volume_mount {
        volume = "apidoc-volume"
        destination = "/var/www/localhost/htdocs"
      }

      service {
        name = "hiive-api-doc-website"
        port = "doc-http"

        check {
          type     = "http"
          path     = "/doc"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }
  }
}
