job "pilelyplantehotel-website" {
  datacenters = ["dc1"]

  group "pilelyplantehotel-website" {
    count = 1

    network {
      port "http" {}
    }

    task "pilelyplantehotel-website" {
      driver = "docker"

      config {
        image = "yen3k/pilelyplantehotel-website:latest"
        ports = ["http"]
        tty   = true
      }

      env {
        WEB_PORT = NOMAD_PORT_http
      }

      service {
        name = "pilelyplantehotel-website"
        port = "http"

        check {
          type     = "http"
          path     = "/"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }
  }
}
