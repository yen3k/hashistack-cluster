job "minihelp-playground" {
  datacenters = ["dc1"]

  group "minihelp-playground" {
    count = 1

    network {
      port "http" {
        static = 80
      }
    }

    task "minihelp-playground" {
      driver = "docker"

      config {
        image = "yen3k/minihelp-playground:0.3"
        ports = ["http"]
        tty   = true
      }

      service {
        name = "minihelp-playground"
        port = "http"

        check {
          type     = "http"
          path     = "/"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }
  }
}
