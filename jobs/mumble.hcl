job "mumble" {

  datacenters = ["dc1"]

  group "mumble" {
    count = 1

    network {
      port "data" { static = 64738 }
    }

    volume "mumble-volume" {
      type = "host"
      source = "mumble-volume"
    }

    vault {
      policies = ["default", "mumble"]
    }

    task "mumble" {
      driver = "docker"

      constraint {
        attribute = "${attr.unique.hostname}"
        operator = "=="
        value = "cluster-c3"
      }

      config {
        image = "yen3k/mumble-server:latest"
        ports = ["data"]
      }

      template {
        data = <<EOF
MUMBLE_CONFIG_SERVER_PASSWORD="{{ with secret "secret/data/mumble" }}{{ .Data.data.password }}{{ end }}"
MUMBLE_SUPERUSER_PASSWORD="{{ with secret "secret/data/mumble" }}{{ .Data.data.adminpassword }}{{ end }}"
MUMBLE_CONFIG_USERS="10"
MUMBLE_CONFIG_ALLOW_PING="false"
MUMBLE_CONFIG_WELCOME_TEXT_FILE="/data/welcome.txt"
MUMBLE_CONFIG_REGISTER_NAME="Guts and Ropeses"
EOF
        destination = "secrets/file.env"
        env         = true
      }

      volume_mount {
        volume = "mumble-volume"
        destination = "/data"
      }

      vault {
        policies = ["default", "mumble"]
      }

      service {
        name = "mumble"
        tags = ["gar"]
        port = "data"
      }

      resources {
        cpu = 1000
        memory = 2048
      }
    }
  }
}
