job "dev-hiive-sensors" {

  datacenters = ["dc1"]

  type = "batch"

  periodic {
    cron             = "*/30 * * * * *"
    prohibit_overlap = true
  }
  group "sensors" {
    count = 1

    task "sensor-70e32da0a6e8" {
      driver = "docker"

      config {
        image = "yen3k/hiive-sensor-mock:latest"
      }

      vault {
        policies = ["default", "haproxy"]
      }

      template {
        data        = <<EOF
SENSOR=124121025390312
GATEWAY=81540272576663
TOKEN=d9c192e4-d3f9-4e32-99fe-9211be67abf3
DEV_HIIVE_USER="{{ with secret "secret/data/haproxy" }}{{ .Data.data.devhiiveuser }}{{ end }}"
DEV_HIIVE_PASS="{{ with secret "secret/data/haproxy" }}{{ .Data.data.devhiivepass }}{{ end }}"
EOF
        destination = "secrets/file.env"
        env         = true
      }
    }

    task "sensor-d444ca03eb46" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "!="
        value     = "cluster-s1"
      }

      config {
        image = "yen3k/hiive-sensor-mock:latest"
      }

      vault {
        policies = ["default", "haproxy"]
      }

      template {
        data        = <<EOF
SENSOR=233391912119110
GATEWAY=141969327142
TOKEN=20ad677e-549f-4016-a608-7cba8fbc7369
DEV_HIIVE_USER="{{ with secret "secret/data/haproxy" }}{{ .Data.data.devhiiveuser }}{{ end }}"
DEV_HIIVE_PASS="{{ with secret "secret/data/haproxy" }}{{ .Data.data.devhiivepass }}{{ end }}"
EOF
        destination = "secrets/file.env"
        env         = true
      }
    }
  }
}
