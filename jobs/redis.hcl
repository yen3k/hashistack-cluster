job "redis" {

  datacenters = ["dc1"]

  group "redis" {
    count = 1

    network {
      port "redis" { static = 6379 }
    }

    task "redis" {
      driver = "docker"

      config {
        image = "redis:7-alpine"
        ports = ["redis"]
      }

      service {
        name = "redis"
        tags = ["hiive"]
        port = "redis"

        check {
          name     = "alive"
          type     = "tcp"
          interval = "30s"
          timeout  = "2s"
        }
      }

      resources {
        cpu    = 500
        memory = 500
      }
    }
  }
}
