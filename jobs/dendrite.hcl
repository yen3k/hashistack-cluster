job "dendrite" {

  datacenters = ["dc1"]

  group "dendrite" {
    count = 1

    network {
      port "https" {
        to = 8008
      }
    }

    volume "dendrite-volume" {
      type   = "host"
      source = "dendrite-volume"
    }

    task "dendrite" {
      driver = "docker"

      config {
        image = "matrixdotorg/dendrite-monolith:v0.8.2"
        ports = ["https"]

        args = [
          "--tls-cert=/etc/dendrite/server.crt",
          "--tls-key=/etc/dendrite/server.key"
        ]

        volumes = [
          "local/dendrite.yaml:/etc/dendrite/dendrite.yaml",
          "local/matrix_key.pem:/etc/dendrite/matrix_key.pem",
          "local/server.crt:/etc/dendrite/server.crt",
          "local/server.key:/etc/dendrite/server.key",
        ]
      }

      template {
        data        = <<EOF
version: 2

global:
  server_name: yenting.party
  private_key: /etc/dendrite/matrix_key.pem
  key_validity_period: 168h0m0s
  well_known_server_name: "matrix.yenting.party"
  trusted_third_party_id_servers:
  - matrix.org
  - vector.im

app_service_api:
  internal_api:
    listen: http://0.0.0.0:7777
    connect: http://appservice_api:7777
  database:
    connection_string: postgresql://dendrite:{{ with secret "secret/data/dendrite" }}{{ .Data.data.pgpassword }}{{ end }}@{{ range service "postgres"}}{{.Address}}{{end}}/dendrite_appservice?sslmode=disable
    max_open_conns: 10
    max_idle_conns: 2
    conn_max_lifetime: -1

client_api:
  internal_api:
    listen: http://0.0.0.0:7771
    connect: http://client_api:7771
  external_api:
    listen: http://0.0.0.0:8071
  registration_disabled: true
  registration_shared_secret: "{{ with secret "secret/data/dendrite" }}{{ .Data.data.accountcreationsecret }}{{ end }}"

edu_server:
  internal_api:
    listen: http://0.0.0.0:7778
    connect: http://edu_server:7778

federation_api:
  internal_api:
    listen: http://0.0.0.0:7772
    connect: http://federation_api:7772
  external_api:
    listen: http://0.0.0.0:8072
  database:
    connection_string: postgresql://dendrite:{{ with secret "secret/data/dendrite" }}{{ .Data.data.pgpassword }}{{ end }}@{{ range service "postgres"}}{{.Address}}{{end}}/dendrite_federationapi?sslmode=disable
    max_open_conns: 10
    max_idle_conns: 2
    conn_max_lifetime: -1
  proxy_outbound:
    enabled: false
    protocol: http
    host: localhost
    port: 8080
  key_perspectives:
  - server_name: matrix.org
    keys:
    - key_id: ed25519:auto
      public_key: Noi6WqcDj0QmPxCNQqgezwTlBKrfqehY1u2FyWP9uYw
    - key_id: ed25519:a_RXGa
      public_key: l8Hft5qXKn1vfHrg3p4+W8gELQVo8N13JkluMfmn2sQ

key_server:
  internal_api:
    listen: http://0.0.0.0:7779
    connect: http://key_server:7779
  database:
    connection_string: postgresql://dendrite:{{ with secret "secret/data/dendrite" }}{{ .Data.data.pgpassword }}{{ end }}@{{ range service "postgres"}}{{.Address}}{{end}}/dendrite_keyserver?sslmode=disable
    max_open_conns: 10
    max_idle_conns: 2
    conn_max_lifetime: -1

media_api:
  internal_api:
    listen: http://0.0.0.0:7774
    connect: http://media_api:7774
  external_api:
    listen: http://0.0.0.0:8074
  database:
    connection_string: postgresql://dendrite:{{ with secret "secret/data/dendrite" }}{{ .Data.data.pgpassword }}{{ end }}@{{ range service "postgres"}}{{.Address}}{{end}}/dendrite_mediaapi?sslmode=disable
    max_open_conns: 10
    max_idle_conns: 2
    conn_max_lifetime: -1
  base_path: /var/dendrite/media
  max_file_size_bytes: 10485760

  # Whether to dynamically generate thumbnails if needed.
  dynamic_thumbnails: false

  # The maximum number of simultaneous thumbnail generators to run.
  max_thumbnail_generators: 10

  # A list of thumbnail sizes to be generated for media content.
  thumbnail_sizes:
  - width: 32
    height: 32
    method: crop
  - width: 96
    height: 96
    method: crop
  - width: 640
    height: 480
    method: scale

mscs:
  mscs: []
  database:
    connection_string: postgresql://dendrite:{{ with secret "secret/data/dendrite" }}{{ .Data.data.pgpassword }}{{ end }}@{{ range service "postgres"}}{{.Address}}{{end}}/dendrite_mscs?sslmode=disable
    max_open_conns: 5
    max_idle_conns: 2
    conn_max_lifetime: -1

room_server:
  internal_api:
    listen: http://0.0.0.0:7770
    connect: http://room_server:7770
  database:
    connection_string: postgresql://dendrite:{{ with secret "secret/data/dendrite" }}{{ .Data.data.pgpassword }}{{ end }}@{{ range service "postgres"}}{{.Address}}{{end}}/dendrite_roomserver?sslmode=disable
    max_open_conns: 10
    max_idle_conns: 2
    conn_max_lifetime: -1

sync_api:
  internal_api:
    listen: http://0.0.0.0:7773
    connect: http://sync_api:7773
  external_api:
      listen: http://0.0.0.0:8073
  database:
    connection_string: postgresql://dendrite:{{ with secret "secret/data/dendrite" }}{{ .Data.data.pgpassword }}{{ end }}@{{ range service "postgres"}}{{.Address}}{{end}}/dendrite_syncapi?sslmode=disable
    max_open_conns: 10
    max_idle_conns: 2
    conn_max_lifetime: -1

user_api:
  internal_api:
    listen: http://0.0.0.0:7781
    connect: http://user_api:7781
  account_database:
    connection_string: postgresql://dendrite:{{ with secret "secret/data/dendrite" }}{{ .Data.data.pgpassword }}{{ end }}@{{ range service "postgres"}}{{.Address}}{{end}}/dendrite_userapi_accounts?sslmode=disable
    max_open_conns: 10
    max_idle_conns: 2
    conn_max_lifetime: -1
  device_database:
    connection_string: postgresql://dendrite:{{ with secret "secret/data/dendrite" }}{{ .Data.data.pgpassword }}{{ end }}@{{ range service "postgres"}}{{.Address}}{{end}}/dendrite_userapi_devices?sslmode=disable
    max_open_conns: 10
    max_idle_conns: 2
    conn_max_lifetime: -1

logging:
- type: file
  level: info
  params:
    path: /var/log/dendrite
EOF
        destination = "local/dendrite.yaml"
      }

      template {
        data        = <<EOF
{{ with secret "secret/data/dendrite" }}{{ .Data.data.privatekey }}{{ end }}
EOF
        destination = "local/matrix_key.pem"
      }

      template {
        data        = <<EOF
{{ with secret "secret/data/dendrite" }}{{ .Data.data.cert }}{{ end }}
EOF
        destination = "local/server.crt"
      }

      template {
        data        = <<EOF
{{ with secret "secret/data/dendrite" }}{{ .Data.data.key }}{{ end }}
EOF
        destination = "local/server.key"
      }

      volume_mount {
        volume      = "dendrite-volume"
        destination = "/var/dendrite/media"
      }

      vault {
        policies = ["default", "dendrite"]
      }

      service {
        name = "matrix"
        port = "https"
        tags = ["sesh", "matrix"]

      }
    }
  }
}
