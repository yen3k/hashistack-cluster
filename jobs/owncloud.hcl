job "owncloud" {
  datacenters = ["dc1"]

  group "owncloud" {
    count = 1

    network {
      port "http" {
        to = 8080
      }
    }

    volume "owncloud-volume" {
      type   = "host"
      source = "owncloud-volume"
    }

    update {
      healthy_deadline  = "10m"
      progress_deadline = "15m"
    }

    task "owncloud" {
      driver = "docker"

      config {
        image = "owncloud/server:10.12.2"
        ports = ["http"]
      }

      template {
        data        = <<EOF
OWNCLOUD_TRUSTED_DOMAINS="bzzzzcloud.hiive.eu"
EOF
        destination = "secrets/file.env"
        env         = true
      }

      volume_mount {
        volume      = "owncloud-volume"
        destination = "/mnt/data"
      }

      service {
        name = "owncloud"
        tags = ["hiive"]
        port = "http"

        check {
          type     = "tcp"
          port     = "http"
          interval = "30s"
          timeout  = "4s"
        }
      }

      resources {
        cpu    = 2000
        memory = 2000
      }
    }
  }
}
