job "gitea" {
  datacenters = ["dc1"]

  group "gitea" {
    count = 1

    volume "gitea-volume" {
      type   = "host"
      source = "gitea-volume"
    }

    network {
      port "http" {}
    }

    task "gitea" {
      driver = "docker"

      config {
        image = "gitea/gitea:1.21.10"
        ports = ["http"]
      }

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = "cluster-s1"
      }

      env {
        GITEA__DEFAULT__APP_NAME                = "SESH"
        GITEA__database__DB_TYPE                = "postgres"
        GITEA__database__NAME                   = "gitea"
        GITEA__database__USER                   = "gitea"
        GITEA__database__SSL_MODE               = "disable"
        GITEA__server__PROTOCOL                 = "http"
        GITEA__server__DOMAIN                   = "gitea.sesh.jetzt"
        GITEA__server__ROOT_URL                 = "https://gitea.sesh.jetzt/"
        GITEA__server__HTTP_PORT                = NOMAD_PORT_http
        GITEA__server__LFS_START_SERVER         = true
        GITEA__lfs__STORAGE_TYPE                = "local"
        GITEA__lfs__PATH                        = "/data/lfs/"
        GITEA__mailer__ENABLED                  = false
        GITEA__service__REGISTER_EMAIL_CONFIRM  = false
        GITEA__service__REGISTER_MANUAL_CONFIRM = true
        GITEA__security__INSTALL_LOCK           = true # Set to false on first install
      }

      template {
        data        = <<EOF
GITEA__database__HOST="{{ range service "postgres"}}{{.Address}}{{ end }}:{{ range service "postgres"}}{{.Port}}{{ end }}"
GITEA__database__PASSWD="{{ with secret "secret/data/gitea" }}{{ .Data.data.pgpassword }}{{ end }}"
EOF
        destination = "secrets/file.env"
        env         = true
      }

      volume_mount {
        volume      = "gitea-volume"
        destination = "/data"
      }

      vault {
        policies = ["default", "gitea"]
      }

      service {
        name = "gitea"
        port = "http"

        check {
          type     = "http"
          path     = "/api/healthz"
          interval = "30s"
          timeout  = "2s"
        }
      }

      resources {
        cpu    = 500
        memory = 500
      }

    }
  }
}
