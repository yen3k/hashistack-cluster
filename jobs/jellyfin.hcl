job "jellyfin" {
  datacenters = ["dc1"]

  group "jellyfin" {
    count = 1

    network {
      port "http" {
        to = 8096
      }
    }

    volume "jellyfin-cache-volume" {
      type   = "host"
      source = "jellyfin-cache-volume"
    }

    volume "jellyfin-config-volume" {
      type   = "host"
      source = "jellyfin-config-volume"
    }

    volume "jellyfin-media-volume" {
      type   = "host"
      source = "jellyfin-media-volume"
    }

    task "jellyfin" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = "cluster-s1"
      }

      config {
        image = "jellyfin/jellyfin:10.10.6"
        ports = ["http"]
        tty   = true
      }

      volume_mount {
        volume      = "jellyfin-cache-volume"
        destination = "/cache"
      }

      volume_mount {
        volume      = "jellyfin-config-volume"
        destination = "/config"
      }

      volume_mount {
        volume      = "jellyfin-media-volume"
        destination = "/media"
      }

      service {
        name = "jellyfin"
        port = "http"

        check {
          type     = "http"
          path     = "/health"
          interval = "30s"
          timeout  = "2s"
        }
      }

      resources {
        cpu    = 3500
        memory = 2500
      }
    }
  }
}
