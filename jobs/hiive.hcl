job "hiive" {
  datacenters = ["dc1"]

  group "hiive" {
    count = 1

    network {
      port "http" { }
    }

    task "hiive-api" {
      driver = "docker"

      config {
        image = "yen3k/hiive-api:latest"
        ports = ["http"]
      }

      constraint {
        attribute = "${attr.unique.hostname}"
        operator = "=="
        value = "cluster-s1"
      }

      env {
        APP_ENVIRONMENT="production"
        APP_APPLICATION__PORT ="${NOMAD_PORT_http}"
        APP_DATABASE__DATABASE_NAME="hiive"
        APP_DATABASE__USERNAME="hiive"
        APP_DATABASE__REQUIRE_SSL=false
        APP_APPLICATION__ACCESS_TOKEN_DURATION=5
        APP_APPLICATION__REFRESH_TOKEN_DURATION=10
        APP_APPLICATION__BASE_URL="https://api.hiive.sesh.jetzt"
        APP_EMAIL_CLIENT__SENDER_EMAIL="hiive@yenmail.party"
      }

      template {
        data = <<EOF
APP_APPLICATION__SECRET_KEY="{{ with secret "secret/data/hiive" }}{{ .Data.data.secretkey }}{{ end }}"
APP_DATABASE__PASSWORD="{{ with secret "secret/data/hiive" }}{{ .Data.data.pgpassword }}{{ end }}"
APP_DATABASE__HOST="{{ range service "postgres"}}{{.Address}}{{ end }}"
APP_DATABASE__PORT="{{ range service "postgres"}}{{.Port}}{{ end }}"
APP_EMAIL_CLIENT__AUTHORIZATION_TOKEN="{{ with secret "secret/data/hiive" }}{{ .Data.data.postmarktoken }}{{ end }}"
EOF
        destination = "secrets/file.env"
        env         = true
      }

      vault {
        policies = ["default", "hiive"]
      }

      service {
        name = "hiive-api"
        port = "http"

        check {
          type     = "http"
          path     = "/health_check"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }
  }
}
