job "ostemejeriet-website" {
  datacenters = ["dc1"]

  group "ostemejeriet-website" {
    count = 1

    network {
      port "http" {}
    }

    task "ostemejeriet-website" {
      driver = "docker"

      config {
        image = "yen3k/ostemejeriet-website:latest"
        ports = ["http"]
      }

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = "cluster-s1"
      }

      env {
        APP_ENVIRONMENT       = "production"
        APP_APPLICATION__PORT = NOMAD_PORT_http
        ROOT_DIR              = "/http_root"
      }

      template {
        data        = <<EOF
APP_EMAIL__FROM_EMAIL="{{ with secret "secret/data/ostemejeriet_website" }}{{ .Data.data.from_email }}{{ end }}"
APP_EMAIL__TO_EMAIL="{{ with secret "secret/data/ostemejeriet_website" }}{{ .Data.data.to_email }}{{ end }}"
APP_EMAIL__SMTP_HOST="{{ with secret "secret/data/ostemejeriet_website" }}{{ .Data.data.smtp_host }}{{ end }}"
APP_EMAIL__SMTP_USERNAME="{{ with secret "secret/data/ostemejeriet_website" }}{{ .Data.data.smtp_username }}{{ end }}"
APP_EMAIL__SMTP_PASSWORD="{{ with secret "secret/data/ostemejeriet_website" }}{{ .Data.data.smtp_password }}{{ end }}"
EOF
        destination = "secrets/file.env"
        env         = true
      }

      vault {
        policies = ["default", "ostemejeriet_website"]
      }

      service {
        name = "ostemejeriet-website"
        port = "http"

        check {
          type     = "http"
          path     = "/health_check"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }
  }
}
