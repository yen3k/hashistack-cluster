job "grafana" {
  datacenters = ["dc1"]

  group "grafana" {
    count = 1

    network {
      port "http" {
        static = 3000
      }
    }

    task "grafana" {
      driver = "docker"

      config {
        image = "grafana/grafana-oss:9.2.2"
        ports = ["http"]
      }

      env {
        GF_SERVER_HTTP_PORT = NOMAD_PORT_http
      }

      service {
        name = "grafana"
        port = "http"

        check {
          type     = "http"
          path     = "/api/health"
          interval = "60s"
          timeout  = "2s"
        }
      }
    }
  }
}
