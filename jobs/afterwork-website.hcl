job "afterwork-website" {
  datacenters = ["dc1"]

  group "afterwork-website" {
    count = 1

    network {
      port "http" {}
    }

    task "afterwork-website" {
      driver = "docker"

      config {
        image = "yen3k/afterwork-website:latest"
        ports = ["http"]
        tty   = true
      }

      env {
        WEB_PORT = NOMAD_PORT_http
      }

      service {
        name = "afterwork-website"
        port = "http"

        check {
          type     = "http"
          path     = "/"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }
  }
}
