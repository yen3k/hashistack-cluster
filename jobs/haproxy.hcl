job "haproxy" {
  region      = "global"
  datacenters = ["dc1"]
  type        = "service"

  group "haproxy" {
    count = 1

    volume "certs-volume" {
      type   = "host"
      source = "certs-volume"
    }

    network {
      port "http" {
        static = 8080
      }

      port "https" {
        static = 4443
      }

      port "stats" {
        static = 1936
      }
    }

    vault {
      policies = ["default", "haproxy"]
    }

    service {
      name = "haproxy"

      check {
        name     = "alive"
        type     = "tcp"
        port     = "http"
        interval = "30s"
        timeout  = "2s"
      }
    }

    task "haproxy" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "="
        value     = "cluster-s1"
      }

      config {
        image        = "haproxy:2.7"
        network_mode = "host"

        volumes = [
          "local/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg",
        ]
      }

      template {
        data = <<EOF
global
  ssl-default-bind-ciphers ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256
  ssl-default-bind-options ssl-min-ver TLSv1.2 no-tls-tickets
  fd-hard-limit 50000

defaults
timeout connect 10s
  mode http
  timeout client 30s
  timeout server 30s
  maxconn 3000

userlist yenting_auth
  user {{ with secret "secret/data/haproxy" }}{{ .Data.data.yentinguser }}{{ end }} insecure-password {{ with secret "secret/data/haproxy" }}{{ .Data.data.yentingpass }}{{ end }}

#userlist dev_hiive_auth
  #user {{ with secret "secret/data/haproxy" }}{{ .Data.data.devhiiveuser }}{{ end }} insecure-password {{ with secret "secret/data/haproxy" }}{{ .Data.data.devhiivepass }}{{ end }}

frontend stats
  bind *:{{ env "NOMAD_PORT_stats" }}
  stats uri /
  stats show-legends
  no log

frontend fe_main
  bind *:{{ env "NOMAD_PORT_http" }}
  bind *:{{ env "NOMAD_PORT_https" }} ssl crt /opt/certs/ alpn h2,http/1.1

  ## Ensure HTTPS except for dev.api.hiive.sesh.jetzt/reading
  http-request set-header X-Forwarded-Proto https if { ssl_fc }
  http-request set-header X-Forwarded-Proto http if !{ ssl_fc }
  #http-request redirect scheme https code 301 unless { ssl_fc } OR { path_beg /reading } OR { path_beg /health_check } OR { path_beg /bot } OR { path_end .bin }

  # Special backend rules
  #use_backend api-doc-hiive if { req.hdr(Host) dev.api.hiive.sesh.jetzt } { path_beg /doc }
  #use_backend hiive-bot if { req.hdr(Host) hiive.sesh.jetzt } { path_beg /bot }

  use_backend %[req.hdr(Host),lower,word(1,:)]

backend yenting.party
  http-request redirect code 301 location https://minihelp.yenting.party if { path -i -m beg /minihelp-playground }
  balance roundrobin
  server-template yenting-party 3 _yenting-webserver._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

backend sesh.jetzt
  balance roundrobin
  server-template yenting-party 3 _yenting-webserver._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

backend bzzzzcloud.hiive.eu
  balance roundrobin
  server-template owncloud 1 _owncloud._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

backend gar.sesh.jetzt
  balance roundrobin
  server-template gar-website 3 _gar-website._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

backend afterwork.solutions
  balance roundrobin
  server-template afterwork-website 3 _afterwork-website._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

backend pilelyplantehotel.dk
  balance roundrobin
  server-template pilelyplantehotel-website 3 _pilelyplantehotel-website._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

backend minihelp.yenting.party
  balance roundrobin
  server-template minihelp-playground 3 _minihelp-playground._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

#backend api.hiive.sesh.jetzt
#  balance roundrobin
#  server-template hiive-api 3 _hiive-api._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

#backend dev.api.hiive.sesh.jetzt
#  http-request auth unless { http_auth(dev_hiive_auth) } OR { method OPTIONS } OR { path_end assetlinks.json }
#  balance roundrobin
#  server-template dev-hiive-api 3 _dev-hiive-api._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

#backend dev.readings.hiive.sesh.jetzt
#  http-request auth unless { http_auth(dev_hiive_auth) }
#  balance roundrobin
#  server-template dev-hiive-readings 3 _dev-hiive-readings._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

#backend dev.releases.hiive.sesh.jetzt
#  http-request auth unless { http_auth(dev_hiive_auth) }
#  balance roundrobin
#  server-template dev-hiive-releases-webserver 3 _dev-hiive-releases-webserver._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

#backend api-doc-hiive
#  balance roundrobin
#  server-template hiive-api-doc-website 3 _hiive-api-doc-website._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

backend gitea.sesh.jetzt
  balance roundrobin
  server-template gitea 3 _gitea._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

#backend hiive-bot
#  balance roundrobin
#  server-template hiive-bot 3 _hiive-bot._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

backend yenstriben.sesh.jetzt
  balance roundrobin
  server-template jellyfin 3 _jellyfin._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

backend kolasacompany.dk
  balance roundrobin
  server-template kolasacompany-website 3 _kolasacompany-website._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

backend ostemejeriet.yenting.party
  balance roundrobin
  server-template ostemejeriet-website 3 _ostemejeriet-website._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check

backend riddle.sesh.jetzt
  balance roundrobin
  server-template riddle-website 3 _riddle-website._tcp.service.consul resolvers consul resolve-opts allow-dup-ip resolve-prefer ipv4 check


resolvers consul
  nameserver consul 127.0.0.1:8600
  accepted_payload_size 8192
  hold valid 5s
EOF

        destination = "local/haproxy.cfg"
      }

      volume_mount {
        volume      = "certs-volume"
        destination = "/opt/certs/"
      }

      resources {
        cpu    = 500
        memory = 500
      }
    }
  }
}
