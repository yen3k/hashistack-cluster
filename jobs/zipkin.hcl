job "zipkin" {
  datacenters = ["dc1"]

  group "zipkin" {
    count = 1

    network {
      port "http" {
        static = 9411
      }
    }

    task "zipkin" {
      driver = "docker"

      config {
        image = "openzipkin/zipkin:2.24.3"
        ports = ["http"]
      }

      service {
        name = "zipkin"
        port = "http"

        check {
          type     = "http"
          path     = "/health"
          interval = "30s"
          timeout  = "2s"
        }
      }

      resources {
        cpu    = 200
        memory = 1000
      }
    }
  }
}
