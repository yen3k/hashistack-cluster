job "postgres" {

  datacenters = ["dc1"]

  group "postgres" {
    count = 1

    network {
      port "pg" { static = 5432 }
    }

    volume "postgres-volume" {
      type   = "host"
      source = "postgres-volume"
    }

    vault {
      policies = ["default", "postgres", "hiive", "devhiive", "gitea"]
    }

    task "postgres" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = "cluster-s1"
      }

      config {
        image = "postgres:14.6"
        ports = ["pg"]

        volumes = [
          "local/create_users.sh:/docker-entrypoint-initdb.d/10-create_users.sh",
          "local/create_dbs.sh:/docker-entrypoint-initdb.d/20-create_dbs.sh",
        ]
      }
      template {
        data        = <<EOF
#!/bin/sh
echo 1 > /tmp/createusers
psql -c "CREATE USER hiive WITH PASSWORD '{{ with secret "secret/data/hiive" }}{{ .Data.data.pgpassword }}{{ end }}';"
psql -c "CREATE USER devhiive WITH PASSWORD '{{ with secret "secret/data/devhiive" }}{{ .Data.data.pgpassword }}{{ end }}';"
psql -c "CREATE USER gitea WITH PASSWORD '{{ with secret "secret/data/gitea" }}{{ .Data.data.pgpassword }}{{ end }}';"
EOF
        destination = "local/create_users.sh"
      }

      template {
        data        = <<EOF
#!/bin/sh
echo 1 > /tmp/createdbs
createdb -U postgres -O hiive hiive
createdb -U postgres -O devhiive devhiive
createdb -U postgres -O gitea gitea
EOF
        destination = "local/create_dbs.sh"
      }
      template {
        data        = <<EOF
POSTGRES_PASSWORD="{{ with secret "secret/data/postgres" }}{{ .Data.data.pgpassword }}{{ end }}"
POSTGRES_USER="postgres"
EOF
        destination = "secrets/file.env"
        env         = true
      }

      volume_mount {
        volume      = "postgres-volume"
        destination = "/var/lib/postgresql/data"
      }

      service {
        name = "postgres"
        tags = ["hiive"]
        port = "pg"

        check {
          task     = "postgres"
          type     = "script"
          command  = "pg_isready"
          args     = ["-U", "postgres"]
          interval = "30s"
          timeout  = "5s"
        }
      }

      resources {
        cpu    = 500
        memory = 1000
      }
    }
  }
}
