job "yenting-webserver" {
  datacenters = ["dc1"]

  group "yenting-webserver" {
    count = 1

    network {
      port "http" {}
    }

    volume "yenting-webserver-volume" {
      type   = "host"
      source = "yenting-webserver-volume"
    }

    task "yenting-webserver" {
      driver = "docker"

      config {
        image = "yen3k/yenting-webserver:latest"
        ports = ["http"]
        tty   = true

        volumes = [
          "local/lighttpd.conf:/etc/lighttpd/lighttpd.conf",
        ]
      }

      env {
        WEB_PORT = NOMAD_PORT_http
      }

      volume_mount {
        volume      = "yenting-webserver-volume"
        destination = "/var/www/localhost/htdocs"
      }
      template {
        data = <<EOF
server.document-root = "/var/www/localhost/htdocs/"
server.port          = env.WEB_PORT
index-file.names     = ( "index.html" )
dir-listing.activate = "enable"
mimetype.assign             = (
  ".pdf"          =>      "application/pdf",
  ".sig"          =>      "application/pgp-signature",
  ".spl"          =>      "application/futuresplash",
  ".class"        =>      "application/octet-stream",
  ".ps"           =>      "application/postscript",
  ".torrent"      =>      "application/x-bittorrent",
  ".dvi"          =>      "application/x-dvi",
  ".gz"           =>      "application/x-gzip",
  ".pac"          =>      "application/x-ns-proxy-autoconfig",
  ".swf"          =>      "application/x-shockwave-flash",
  ".tar.gz"       =>      "application/x-tgz",
  ".tgz"          =>      "application/x-tgz",
  ".tar"          =>      "application/x-tar",
  ".zip"          =>      "application/zip",
  ".mp3"          =>      "audio/mpeg",
  ".m3u"          =>      "audio/x-mpegurl",
  ".wma"          =>      "audio/x-ms-wma",
  ".wax"          =>      "audio/x-ms-wax",
  ".ogg"          =>      "application/ogg",
  ".wav"          =>      "audio/x-wav",
  ".gif"          =>      "image/gif",
  ".jpg"          =>      "image/jpeg",
  ".jpeg"         =>      "image/jpeg",
  ".png"          =>      "image/png",
  ".xbm"          =>      "image/x-xbitmap",
  ".xpm"          =>      "image/x-xpixmap",
  ".xwd"          =>      "image/x-xwindowdump",
  ".css"          =>      "text/css",
  ".html"         =>      "text/html",
  ".htm"          =>      "text/html",
  ".js"           =>      "application/javascript",
  ".asc"          =>      "text/plain",
  ".c"            =>      "text/plain",
  ".cpp"          =>      "text/plain",
  ".log"          =>      "text/plain",
  ".conf"         =>      "text/plain",
  ".text"         =>      "text/plain",
  ".txt"          =>      "text/plain",
  ".spec"         =>      "text/plain",
  ".dtd"          =>      "text/xml",
  ".xml"          =>      "text/xml",
  ".mpeg"         =>      "video/mpeg",
  ".mpg"          =>      "video/mpeg",
  ".mov"          =>      "video/quicktime",
  ".qt"           =>      "video/quicktime",
  ".avi"          =>      "video/x-msvideo",
  ".asf"          =>      "video/x-ms-asf",
  ".asx"          =>      "video/x-ms-asf",
  ".wmv"          =>      "video/x-ms-wmv",
  ".bz2"          =>      "application/x-bzip",
  ".tbz"          =>      "application/x-bzip-compressed-tar",
  ".tar.bz2"      =>      "application/x-bzip-compressed-tar",
  ".odt"          =>      "application/vnd.oasis.opendocument.text", 
  ".ods"          =>      "application/vnd.oasis.opendocument.spreadsheet", 
  ".odp"          =>      "application/vnd.oasis.opendocument.presentation", 
  ".odg"          =>      "application/vnd.oasis.opendocument.graphics", 
  ".odc"          =>      "application/vnd.oasis.opendocument.chart", 
  ".odf"          =>      "application/vnd.oasis.opendocument.formula", 
  ".odi"          =>      "application/vnd.oasis.opendocument.image", 
  ".odm"          =>      "application/vnd.oasis.opendocument.text-master", 
  ".ott"          =>      "application/vnd.oasis.opendocument.text-template",
  ".ots"          =>      "application/vnd.oasis.opendocument.spreadsheet-template",
  ".otp"          =>      "application/vnd.oasis.opendocument.presentation-template",
  ".otg"          =>      "application/vnd.oasis.opendocument.graphics-template",
  ".otc"          =>      "application/vnd.oasis.opendocument.chart-template",
  ".otf"          =>      "application/vnd.oasis.opendocument.formula-template",
  ".oti"          =>      "application/vnd.oasis.opendocument.image-template",
  ".oth"          =>      "application/vnd.oasis.opendocument.text-web",

  ".kml"          =>      "application/vnd.google-earth.kml+xml",
  ".kmz"          =>      "application/vnd.google-earth.kmz",

  ".doc"          =>      "application/msword",
  ".dot"          =>      "application/msword",
  ".docx"         =>      "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
  ".dotx"         =>      "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
  ".docm"         =>      "application/vnd.ms-word.document.macroEnabled.12",
  ".dotm"         =>      "application/vnd.ms-word.template.macroEnabled.12",
  ".xls"          =>      "application/vnd.ms-excel",
  ".xlt"          =>      "application/vnd.ms-excel",
  ".xla"          =>      "application/vnd.ms-excel",
  ".xlsx"         =>      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  ".xltx"         =>      "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
  ".xlsm"         =>      "application/vnd.ms-excel.sheet.macroEnabled.12",
  ".xltm"         =>      "application/vnd.ms-excel.template.macroEnabled.12",
  ".xlam"         =>      "application/vnd.ms-excel.addin.macroEnabled.12",
  ".xlsb"         =>      "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
  ".ppt"          =>      "application/vnd.ms-powerpoint",
  ".pot"          =>      "application/vnd.ms-powerpoint",
  ".pps"          =>      "application/vnd.ms-powerpoint",
  ".ppa"          =>      "application/vnd.ms-powerpoint",
  ".pptx"         =>      "application/vnd.openxmlformats-officedocument.presentationml.presentation",
  ".potx"         =>      "application/vnd.openxmlformats-officedocument.presentationml.template",
  ".ppsx"         =>      "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
  ".ppam"         =>      "application/vnd.ms-powerpoint.addin.macroEnabled.12",
  ".pptm"         =>      "application/vnd.ms-powerpoint.presentation.macroEnabled.12",
  ".mkv"         => "video/x-matroska",
  ".mp4"         => "video/mp4",

# make the default mime type application/octet-stream.
  ""              =>      "application/octet-stream",
)
EOF

        destination = "local/lighttpd.conf"
      }

      service {
        name = "yenting-webserver"
        port = "http"

        check {
          type     = "http"
          path     = "/"
          interval = "2m"
          timeout  = "2s"
        }
      }
    }
  }
}
