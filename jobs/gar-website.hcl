job "gar-website" {
  datacenters = ["dc1"]

  group "gar-website" {
    count = 1

    network {
      port "http" {}
    }

    task "gar-website" {
      driver = "docker"

      config {
        image = "yen3k/gar-website:latest"
        ports = ["http"]
        tty   = true
      }

      env {
        WEB_PORT = NOMAD_PORT_http
      }

      service {
        name = "gar-website"
        port = "http"

        check {
          type     = "http"
          path     = "/"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }
  }
}
