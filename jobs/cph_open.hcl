job "cph_open" {
  datacenters = ["dc1"]

  type = "batch"

  periodic {
    crons            = ["0 23 * * *"]
    prohibit_overlap = true
  }

  group "cph_open" {
    count = 1

    volume "yenting-webserver-volume" {
      type   = "host"
      source = "yenting-webserver-volume"
    }

    task "yenting-webserver" {
      driver = "docker"

      config {
        image = "yen3k/cph_open:latest"
        tty   = true
      }

      env {
        LAST_EVENT_ID_PATH      = "/data/cph_open_last_event_id"
        POTENTIAL_CPH_OPEN_PATH = "/data/cph_open_ids"
      }

      volume_mount {
        volume      = "yenting-webserver-volume"
        destination = "/data"
      }
    }
  }
}
