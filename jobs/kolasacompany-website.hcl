job "kolasacompany-website" {
  datacenters = ["dc1"]

  group "kolasacompany-website" {
    count = 1
    constraint {
      distinct_hosts = true
    }

    volume "kolasacompany-website-volume" {
      type   = "host"
      source = "kolasacompany-website-volume"
    }

    network {
      port "http" {}
    }

    task "kolasacompany-website" {
      driver = "docker"

      config {
        image = "yen3k/yenting-webserver:latest"
        ports = ["http"]
        tty   = true
      }

      env {
        WEB_PORT = NOMAD_PORT_http
      }

      volume_mount {
        volume      = "kolasacompany-website-volume"
        destination = "/var/www/localhost/htdocs"
      }

      service {
        name = "kolasacompany-website"
        port = "http"

        check {
          type     = "http"
          path     = "/"
          interval = "30s"
          timeout  = "2s"
        }
      }
      resources {
        cpu    = 300
        memory = 512
      }
    }
  }
}
