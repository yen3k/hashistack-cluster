job "sesh-remindme" {

  datacenters = ["dc1"]

  group "sesh-remindme" {
    count = 1

    volume "sesh-remindme-volume" {
      type   = "host"
      source = "sesh-remindme-volume"
    }

    task "sesh-remindme" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = "cluster-s1"
      }

      config {
        image = "yen3k/sesh-remindme:latest"
      }

      template {
        data        = <<EOF
BOTKEY="{{ with secret "secret/data/seshremindme" }}{{ .Data.data.token }}{{ end }}"
EOF
        destination = "secrets/file.env"
        env         = true
      }

      volume_mount {
        volume      = "sesh-remindme-volume"
        destination = "/var/lib/seshremindme"
      }

      vault {
        policies = ["default", "sesh-remindme"]
      }

      service {
        name = "sesh-remindme"
        tags = ["sesh"]
      }
    }
  }
}
