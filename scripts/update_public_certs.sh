# Run as root and dont have anything listening on port 80 and 443
certbot renew --force-renewal
while read cert; do
  cat /etc/letsencrypt/live/${cert}/fullchain.pem /etc/letsencrypt/live/${cert}/privkey.pem | tee /mnt/storage/certs/${cert}.pem
done < active_public_certs
