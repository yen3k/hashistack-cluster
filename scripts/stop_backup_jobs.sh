for job in $(tac backup_jobs)
do
  nomad job stop -purge "$job"
done
