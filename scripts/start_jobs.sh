while read job; do
  nomad job run /mnt/storage/hashistack-cluster/jobs/${job}.hcl
done < active_jobs
