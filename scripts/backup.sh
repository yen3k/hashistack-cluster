mkdir -p /tmp/vault/
consul snapshot save /tmp/vault/vault_backup.snap
rdiff-backup -v5 /tmp/vault/ /mnt/backup/vault/
rm -r /tmp/vault/

./stop_backup_jobs.sh
while read backup_dir; do
  /root/.local/bin/rdiff-backup -v5 --print-statistics /mnt/storage/${backup_dir}/ /mnt/backup/${backup_dir}/
done < backup_dirs
./start_backup_jobs.sh
