for job in $(tac active_jobs)
do
  nomad job stop -purge "$job"
done
